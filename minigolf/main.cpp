#include "struct.cpp"
#include "Grapic.h"
using namespace std;



const int DIM = 900;

const float DT = 0.008;


const float HOMEPAGE_GRAVITY = -0.05;


const int MAX_PULLVEC_LENGTH = 150;
const int MIN_PULLVEC_COLOR_R = 255;
const int MIN_PULLVEC_COLOR_G = 255;
const int MIN_PULLVEC_COLOR_B = 0;
const int MAX_PULLVEC_COLOR_R = 217;
const int MAX_PULLVEC_COLOR_G = 38;
const int MAX_PULLVEC_COLOR_B = 22;


const float MAX_PLAYER_FORCE = 300.0;





void draw_user_line(Vec2 user_aim_source) {
    int x, y;
    mousePos(x, y);

    Vec2 pull_vec = make_vec2(x, y) - user_aim_source;
    if (norm(pull_vec) > MAX_PULLVEC_LENGTH) {
        pull_vec = 150 * normalized(pull_vec);
    }

    int r = MIN_PULLVEC_COLOR_R + (MAX_PULLVEC_COLOR_R - MIN_PULLVEC_COLOR_R) * (norm(pull_vec) / 150);
    int g = MIN_PULLVEC_COLOR_G + (MAX_PULLVEC_COLOR_G - MIN_PULLVEC_COLOR_G) * (norm(pull_vec) / 150);
    int b = MIN_PULLVEC_COLOR_B + (MAX_PULLVEC_COLOR_B - MIN_PULLVEC_COLOR_B) * (norm(pull_vec) / 150);

    color(r, g, b);
    fat_line(user_aim_source.x, user_aim_source.y, user_aim_source.x + pull_vec.x, user_aim_source.y + pull_vec.y, 3);
}





Vec2 compute_applied_force(Vec2 user_aim_source) {
    int x, y;
    mousePos(x, y);

    Vec2 pull_vec = make_vec2(x, y) - user_aim_source;
    float force = norm(pull_vec) / MAX_PULLVEC_LENGTH; // between 0 and 1
    if (force > 1) {force = 1;}

    pull_vec = normalized(pull_vec);

    return (-1) * pull_vec * force * MAX_PLAYER_FORCE;
}



// The user can play when the ball is stopped, is not in a forcefield and is not colliding with a wall.
// (a ball can have 0 velocity when being braked by a forcefield, but
// it is not a permanent state so the player can't play before the ball left the ff)
bool user_can_play(Course c) {
    return norm(c.ball.s) == 0 && norm(c.ball.f) == 0 && !c.ball.is_colliding;
}


















// COURSES DATA   --------------------------------------------------------------------------




const int COURSE_NUMBER = 9;






// Create and return the first course
Course course_1() {
    Course c;
    Image bg = image("images/hole_1.png");
    init(c, make_vec2(450, 200), make_vec2(450, 700), bg, 1);

    Wall w;
    init(w, make_vec2(600, 50), make_vec2(300, 50), 6); add_wall(c, w);
    init(w, make_vec2(300, 50), make_vec2(300, 700), 6); add_wall(c, w);
    init(w, make_vec2(300, 700), make_vec2(350, 775), 6); add_wall(c, w);
    init(w, make_vec2(350, 775), make_vec2(430, 820), 6); add_wall(c, w);
    init(w, make_vec2(430, 820), make_vec2(470, 820), 6); add_wall(c, w);
    init(w, make_vec2(470, 820), make_vec2(550, 775), 6); add_wall(c, w);
    init(w, make_vec2(550, 775), make_vec2(600, 700), 6); add_wall(c, w);
    init(w, make_vec2(600, 700), make_vec2(600, 50), 6); add_wall(c, w);



    MovingPart m;
    Image img = image("images/objects/hole1_obstacle.png");
    init(m, img, 137, 12, make_vec2(340, 450), false, 30, 0);
    m.angle = 90;
    add_point(m, make_vec2(560, 450));
    init(w, make_vec2(340, 512.5), make_vec2(340, 387.5), 6); add_wall(m, w);
    add_moving_part(c, m);


    return c;
}





// Create and return the second course
Course course_2() {
    Course c;
    Image bg = image("images/hole_2.png");
    init(c, make_vec2(273, 231), make_vec2(596, 231), bg, 2);

    Wall w;
    init(w, make_vec2(157, 733), make_vec2(157, 138), 6); add_wall(c, w);
    init(w, make_vec2(157, 138), make_vec2(389, 138), 6); add_wall(c, w);
    init(w, make_vec2(389, 138), make_vec2(389, 503), 6); add_wall(c, w);
    init(w, make_vec2(389, 503), make_vec2(479, 503), 6); add_wall(c, w);
    init(w, make_vec2(479, 503), make_vec2(479, 231), 6); add_wall(c, w);
    init(w, make_vec2(479, 231), make_vec2(518, 173), 6); add_wall(c, w);
    init(w, make_vec2(518, 173), make_vec2(580, 138), 6); add_wall(c, w);
    init(w, make_vec2(580, 138), make_vec2(611, 138), 6); add_wall(c, w);
    init(w, make_vec2(611, 138), make_vec2(673, 173), 6); add_wall(c, w);
    init(w, make_vec2(673, 173), make_vec2(711, 231), 6); add_wall(c, w);
    init(w, make_vec2(711, 231), make_vec2(711, 733), 6); add_wall(c, w);
    init(w, make_vec2(711, 733), make_vec2(157, 733), 6); add_wall(c, w);


    MovingPart m;
    Image img = image("images/objects/hole1_obstacle.png");
    init(m, img, 137, 12, make_vec2(435, 650), false, 0, 30);

    init(w, make_vec2(366.5, 650), make_vec2(503.5, 650), 6); add_wall(m, w);

    add_moving_part(c, m);


    return c;
}




// Create and return the third course
Course course_3() {
    Course c;
    Image bg = image("images/hole_3.png");
    init(c, make_vec2(194, 243), make_vec2(704, 656), bg, 3);

    Wall w;
    init(w, make_vec2(91, 742), make_vec2(91, 158), 6); add_wall(c, w);
    init(w, make_vec2(91, 158), make_vec2(809, 158), 6); add_wall(c, w);
    init(w, make_vec2(809, 158), make_vec2(809, 742), 6); add_wall(c, w);
    init(w, make_vec2(809, 742), make_vec2(91, 742), 6); add_wall(c, w);

    return c;
}





// Create and return the fourth course
Course course_4() {
    Course c;
    Image bg = image("images/hole_4.png");
    init(c, make_vec2(450, 170), make_vec2(450, 700), bg, 4);

    Wall w;
    init(w, make_vec2(192, 91), make_vec2(708, 91), 6); add_wall(c, w);
    init(w, make_vec2(708, 91), make_vec2(708, 343), 6); add_wall(c, w);
    init(w, make_vec2(708, 343), make_vec2(603, 343), 6); add_wall(c, w);
    init(w, make_vec2(603, 343), make_vec2(603, 557), 6); add_wall(c, w);
    init(w, make_vec2(603, 557), make_vec2(708, 557), 6); add_wall(c, w);
    init(w, make_vec2(708, 557), make_vec2(708, 809), 6); add_wall(c, w);
    init(w, make_vec2(708, 809), make_vec2(192, 809), 6); add_wall(c, w);
    init(w, make_vec2(192, 809), make_vec2(192, 557), 6); add_wall(c, w);
    init(w, make_vec2(192, 557), make_vec2(297, 557), 6); add_wall(c, w);
    init(w, make_vec2(297, 557), make_vec2(297, 343), 6); add_wall(c, w);
    init(w, make_vec2(297, 343), make_vec2(192, 343), 6); add_wall(c, w);
    init(w, make_vec2(192, 343), make_vec2(192, 91), 6); add_wall(c, w);

    Forcefield ff;
    init(ff, make_vec2(297, 424), make_vec2(297, 362), make_vec2(603, 362), make_vec2(603, 424), make_vec2(0, -100));
    add_forcefield(c, ff);
    init(ff, make_vec2(297, 538), make_vec2(297, 476), make_vec2(603, 476), make_vec2(603, 538), make_vec2(0, 100));
    add_forcefield(c, ff);


    return c;
}











// Create and return the fifth course
Course course_5() {
    Course c;
    Image bg = image("images/hole_5.png");
    init(c, make_vec2(340, 228), make_vec2(542, 228), bg, 5);

    Wall w;
    init(w, make_vec2(405, 303), make_vec2(405, 156), 6); add_wall(c, w);
    init(w, make_vec2(405, 156), make_vec2(140, 156), 6); add_wall(c, w);
    init(w, make_vec2(140, 156), make_vec2(140, 510), 6); add_wall(c, w);
    init(w, make_vec2(140, 510), make_vec2(450, 744), 6); add_wall(c, w);
    init(w, make_vec2(450, 744), make_vec2(760, 510), 6); add_wall(c, w);
    init(w, make_vec2(760, 510), make_vec2(760, 156), 6); add_wall(c, w);
    init(w, make_vec2(760, 156), make_vec2(495, 156), 6); add_wall(c, w);
    init(w, make_vec2(495, 156), make_vec2(495, 303), 6); add_wall(c, w);
    init(w, make_vec2(495, 303), make_vec2(405, 303), 6); add_wall(c, w);

    init(w, make_vec2(450, 412), make_vec2(450, 643), 6); add_wall(c, w);

    init(w, make_vec2(495, 303), make_vec2(549, 364), 6); add_wall(c, w);
    init(w, make_vec2(405, 303), make_vec2(351, 364), 6); add_wall(c, w);


    Forcefield ff;
    init(ff, make_vec2(140, 199), make_vec2(205, 199), make_vec2(205, 473), make_vec2(140, 473), make_vec2(0, 100));
    add_forcefield(c, ff);

    init(ff, make_vec2(695, 199), make_vec2(760, 199), make_vec2(760, 473), make_vec2(695, 473), make_vec2(0, -100));
    add_forcefield(c, ff);


    MovingPart m;
    init(m, image("images/objects/smoke.png"), 42, 45, make_vec2(247, 693), true, 40, 0);
    add_point(m, make_vec2(247, 950));
    add_moving_part(c, m);

    init(m, image("images/objects/smoke.png"), 42, 45, make_vec2(247, 693), true, 40, 0);
    add_point(m, make_vec2(247, 950));
    m.advancement = 128;
    add_moving_part(c, m);



    return c;
}








// Create and return the sixth course
Course course_6() {
    Course c;
    Image bg = image("images/hole_6.png");
    init(c, make_vec2(223, 225), make_vec2(680, 605), bg, 6);

    Wall w;
    init(w, make_vec2(226, 111), make_vec2(376, 197), 6); add_wall(c, w);
    init(w, make_vec2(376, 197), make_vec2(525, 111), 6); add_wall(c, w);
    init(w, make_vec2(525, 111), make_vec2(674, 197), 6); add_wall(c, w);
    init(w, make_vec2(674, 197), make_vec2(674, 370), 6); add_wall(c, w);
    init(w, make_vec2(674, 370), make_vec2(823, 456), 6); add_wall(c, w);
    init(w, make_vec2(823, 456), make_vec2(823, 628), 6); add_wall(c, w);
    init(w, make_vec2(823, 628), make_vec2(674, 715), 6); add_wall(c, w);
    init(w, make_vec2(674, 715), make_vec2(525, 628), 6); add_wall(c, w);
    init(w, make_vec2(525, 628), make_vec2(376, 715), 6); add_wall(c, w);
    init(w, make_vec2(376, 715), make_vec2(226, 628), 6); add_wall(c, w);
    init(w, make_vec2(226, 628), make_vec2(226, 456), 6); add_wall(c, w);
    init(w, make_vec2(226, 456), make_vec2(77, 370), 6); add_wall(c, w);
    init(w, make_vec2(77, 370), make_vec2(77, 197), 6); add_wall(c, w);
    init(w, make_vec2(77, 197), make_vec2(226, 111), 6); add_wall(c, w);

    init(w, make_vec2(376, 197), make_vec2(376, 395), 6); add_wall(c, w);
    init(w, make_vec2(525, 431), make_vec2(525, 628), 6); add_wall(c, w);


    MovingPart m;
    init(m, image("images/objects/hole_7_obstacle.png"), 279, 279, make_vec2(376, 540), true, 0, 25);
    init(w, make_vec2(263, 476), make_vec2(376, 541), 5); add_wall(m, w);
    init(w, make_vec2(488, 476), make_vec2(376, 541), 5); add_wall(m, w);
    init(w, make_vec2(376, 672), make_vec2(376, 541), 5); add_wall(m, w);
    add_moving_part(c, m);

    init(m, image("images/objects/hole_7_obstacle.png"), 279, 279, make_vec2(524, 282), true, 0, -35);
    init(w, make_vec2(411, 219), make_vec2(524, 284), 5); add_wall(m, w);
    init(w, make_vec2(636, 219), make_vec2(524, 284), 5); add_wall(m, w);
    init(w, make_vec2(524, 414), make_vec2(524, 284), 5); add_wall(m, w);

    add_moving_part(c, m);



    return c;
}




// Create and return the seventh course
Course course_7() {
    Course c;
    Image bg = image("images/hole_7.png");
    init(c, make_vec2(513, 709), make_vec2(513, 581), bg, 7);

    Wall w;
    init(w, make_vec2(450, 793), make_vec2(699, 690), 6); add_wall(c, w);
    init(w, make_vec2(699, 690), make_vec2(802, 441), 6); add_wall(c, w);
    init(w, make_vec2(802, 441), make_vec2(699, 192), 6); add_wall(c, w);
    init(w, make_vec2(699, 192), make_vec2(450, 89), 6); add_wall(c, w);
    init(w, make_vec2(450, 89), make_vec2(201, 192), 6); add_wall(c, w);
    init(w, make_vec2(201, 192), make_vec2(98, 441), 6); add_wall(c, w);
    init(w, make_vec2(98, 441), make_vec2(201, 690), 6); add_wall(c, w);
    init(w, make_vec2(201, 690), make_vec2(450, 793), 6); add_wall(c, w);
    init(w, make_vec2(450, 793), make_vec2(450, 542), 6); add_wall(c, w);
    init(w, make_vec2(450, 542), make_vec2(379, 513), 6); add_wall(c, w);
    init(w, make_vec2(450, 668), make_vec2(610, 601), 6); add_wall(c, w);
    init(w, make_vec2(610, 601), make_vec2(676, 441), 6); add_wall(c, w);
    init(w, make_vec2(697, 192), make_vec2(610, 281), 6); add_wall(c, w);
    init(w, make_vec2(610, 281), make_vec2(379, 370), 6); add_wall(c, w);
    init(w, make_vec2(290, 601), make_vec2(224, 441), 6); add_wall(c, w);
    init(w, make_vec2(224, 441), make_vec2(290, 281), 6); add_wall(c, w);
    init(w, make_vec2(290, 281), make_vec2(450, 215), 6); add_wall(c, w);
    init(w, make_vec2(224, 441), make_vec2(551, 441), 6); add_wall(c, w);
    init(w, make_vec2(521, 370), make_vec2(551, 441), 6); add_wall(c, w);
    init(w, make_vec2(521, 513), make_vec2(551, 441), 6); add_wall(c, w);

    Forcefield ff;
    init(ff, make_vec2(98, 441), make_vec2(228, 441), make_vec2(290, 601), make_vec2(202, 691), make_vec2(28.5, 71.25));
    add_forcefield(c, ff);
    init(ff, make_vec2(98, 441), make_vec2(194, 192), make_vec2(290, 283), make_vec2(228, 441), make_vec2(28.5, -71.25));
    add_forcefield(c, ff);

    MovingPart m;
    init(m, image("images/objects/hole_8_obstacle.png"), 13, 84, make_vec2(290, 477), true, 26, 0);
    add_point(m, make_vec2(290, 406));
    add_point(m, make_vec2(490, 406));
    add_point(m, make_vec2(490, 477));
    add_point(m, make_vec2(290, 477));
    init(w, make_vec2(290, 513), make_vec2(290, 441), 6); add_wall(m, w);
    add_moving_part(c, m);


    return c;
}




// Create and return the heigth course
Course course_8() {
    Course c;
    Image bg = image("images/hole_8.png");
    init(c, make_vec2(189, 441), make_vec2(464, 632), bg, 8);

    
    Wall w;
    init(w, make_vec2(156, 670), make_vec2(156, 213), 6); add_wall(c, w);
    init(w, make_vec2(156, 213), make_vec2(744, 213), 6); add_wall(c, w);
    init(w, make_vec2(744, 213), make_vec2(744, 670), 6); add_wall(c, w);
    init(w, make_vec2(744, 670), make_vec2(156, 670), 6); add_wall(c, w);

    init(w, make_vec2(429, 213), make_vec2(429, 264), 6); add_wall(c, w);
    init(w, make_vec2(429, 314), make_vec2(429, 670), 6); add_wall(c, w);

    init(w, make_vec2(629, 335), make_vec2(651, 314), 4); add_wall(c, w);
    init(w, make_vec2(672, 335), make_vec2(651, 314), 4); add_wall(c, w);

    init(w, make_vec2(502, 402), make_vec2(524, 380), 4); add_wall(c, w);
    init(w, make_vec2(546, 402), make_vec2(524, 380), 4); add_wall(c, w);

    init(w, make_vec2(607, 527), make_vec2(629, 506), 4); add_wall(c, w);
    init(w, make_vec2(651, 527), make_vec2(629, 506), 4); add_wall(c, w);

    init(w, make_vec2(431, 596), make_vec2(524, 596), 4); add_wall(c, w);
    init(w, make_vec2(546, 623), make_vec2(524, 596), 4); add_wall(c, w);

    Forcefield ff;
    init(ff, make_vec2(429, 271), make_vec2(744, 271), make_vec2(744, 670), make_vec2(429, 670), make_vec2(0, -230));
    add_forcefield(c, ff);

    // We use inverted forcefields to "cancel" the effect of the main forcefield at different places
    init(ff, make_vec2(524, 380), make_vec2(546, 402), make_vec2(524, 424), make_vec2(502, 402), make_vec2(0, 230));
    add_forcefield(c, ff);
    init(ff, make_vec2(650, 314), make_vec2(672, 335), make_vec2(651, 357), make_vec2(629, 335), make_vec2(0, 230));
    add_forcefield(c, ff);
    init(ff, make_vec2(629, 506), make_vec2(651, 527), make_vec2(629, 549), make_vec2(607, 527), make_vec2(0, 230));
    add_forcefield(c, ff);
    init(ff, make_vec2(429, 596), make_vec2(546, 596), make_vec2(546, 670), make_vec2(429, 670), make_vec2(0, 230));
    add_forcefield(c, ff);


    MovingPart m;
    init(m, image("images/objects/hole_9_meteorite.png"), 103, 99, make_vec2(-161, 768), true, 20, -7);
    add_point(m, make_vec2(1200, 850));
    add_moving_part(c, m);


    return c;
}






// Create and return the last course
Course course_9() {
    Course c;
    Image bg = image("images/hole_9.png");
    init(c, make_vec2(450, 110), make_vec2(450, 700), bg, 9);

    Wall w;
    init(w, make_vec2(600, 50), make_vec2(300, 50), 6); add_wall(c, w);
    init(w, make_vec2(300, 50), make_vec2(300, 700), 6); add_wall(c, w);
    init(w, make_vec2(300, 700), make_vec2(350, 775), 6); add_wall(c, w);
    init(w, make_vec2(350, 775), make_vec2(430, 820), 6); add_wall(c, w);
    init(w, make_vec2(430, 820), make_vec2(470, 820), 6); add_wall(c, w);
    init(w, make_vec2(470, 820), make_vec2(550, 775), 6); add_wall(c, w);
    init(w, make_vec2(550, 775), make_vec2(600, 700), 6); add_wall(c, w);
    init(w, make_vec2(600, 700), make_vec2(600, 50), 6); add_wall(c, w);


    MovingPart m;
    Image img = image("images/objects/hole_9_obstacle.png");
    init(m, img, 88, 9, make_vec2(340, 197), false, 10, 0);
    add_point(m, make_vec2(560, 197));
    init(w, make_vec2(296, 197), make_vec2(384, 197), 4);
    add_wall(m, w);
    add_moving_part(c, m);

    init(m, img, 88, 9, make_vec2(340, 238), false, 20, 0);
    add_point(m, make_vec2(560, 238));
    init(w, make_vec2(296, 238), make_vec2(384, 238), 4);
    add_wall(m, w);
    add_moving_part(c, m);

    init(m, img, 88, 9, make_vec2(340, 279), false, 30, 0);
    add_point(m, make_vec2(560, 279));
    init(w, make_vec2(296, 279), make_vec2(384, 279), 4);
    add_wall(m, w);
    add_moving_part(c, m);

    init(m, img, 88, 9, make_vec2(340, 320), false, 40, 0);
    add_point(m, make_vec2(560, 320));
    init(w, make_vec2(296, 320), make_vec2(384, 320), 4);
    add_wall(m, w);
    add_moving_part(c, m);



    img = image("images/objects/hole_9_obstacle_2.png");
    init(m, img, 13, 158, make_vec2(517, 614), false, 0, 20);
    init(w, make_vec2(517, 687), make_vec2(517, 669), 6); add_wall(m, w);
    init(w, make_vec2(517, 559), make_vec2(517, 541), 6); add_wall(m, w);
    add_moving_part(c, m);

    init(m, img, 13, 158, make_vec2(381, 614), false, 0, -20);
    init(w, make_vec2(381, 687), make_vec2(381, 669), 6); add_wall(m, w);
    init(w, make_vec2(381, 559), make_vec2(381, 541), 6); add_wall(m, w);
    add_moving_part(c, m);

    init(m, img, 13, 158, make_vec2(455, 551), false, 0, 25);
    init(w, make_vec2(455, 624), make_vec2(455, 606), 6); add_wall(m, w);
    init(w, make_vec2(455, 496), make_vec2(455, 478), 6); add_wall(m, w);
    add_moving_part(c, m);

    init(m, img, 13, 158, make_vec2(517, 526), false, 0, -35);
    init(w, make_vec2(517, 599), make_vec2(517, 581), 6); add_wall(m, w);
    init(w, make_vec2(517, 471), make_vec2(517, 453), 6); add_wall(m, w);
    add_moving_part(c, m);

    init(m, img, 13, 158, make_vec2(381, 526), false, 0, 40);
    init(w, make_vec2(381, 599), make_vec2(381, 581), 6); add_wall(m, w);
    init(w, make_vec2(381, 471), make_vec2(381, 453), 6); add_wall(m, w);
    add_moving_part(c, m);

    init(m, img, 13, 158, make_vec2(455, 462), false, 0, -40);
    init(w, make_vec2(455, 535), make_vec2(455, 517), 6); add_wall(m, w);
    init(w, make_vec2(455, 407), make_vec2(455, 389), 6); add_wall(m, w);
    add_moving_part(c, m);



    return c;
}








// Return the nth course
Course get_course(int n) {
    switch (n) {
        case 1: return course_1();
        case 2: return course_2();
        case 3: return course_3();
        case 4: return course_4();
        case 5: return course_5();
        case 6: return course_6();
        case 7: return course_7();
        case 8: return course_8();
        case 9: return course_9();

        default: return course_1();
    }
}


















int main(void) {
    // init grapic and rng
    winInit("Minigolf", DIM, DIM);
	backgroundColor(39, 168, 95);
	winClear();


    Image homepage = image("images/homepage.png");
    bool homepage_lock = true;
    float homepage_speed = 0;
    int homepage_y = 0;

    Image bravo = image("images/banderole.png");
    bool bravo_displayed = false;
    float bravo_advancement = 0;


    int current_course = 1;
    int nb_strokes = 0;

    int score = 0;

    Course c = get_course(current_course);


    bool user_aiming = false;
    Vec2 user_aim_source;

    bool debug = false;
    bool ball_grabbed = false;
    Vec2 grab_delta;

    bool test_mode = false;


    bool stop = false;
    while (!stop) {
        winClear();


        // Store the mouse coordinates
        int x, y;
        mousePos(x, y);
        Vec2 mouse_pos = make_vec2(x, y);


        // Draw game
        draw(c, debug);

        // Draw game interface
        color(255, 255, 255);
        fontSize(40);
        print(20, 840, "Trou");
        print(110, 840, c.course_nb);

        fontSize(20);

        if (test_mode) {
            print(700, 855, "MODE TRANQUILLE");
        }
        else {
            print(20, 815, "Nombre de coups:");
            print(190, 815, nb_strokes);

            print(20, 790, "Score:");
            print(80, 790, score);
        }


        // Print more debug information not related to the Course struct
        if (debug) {
            color(255, 255, 255);
            fontSize(15);
            
            if (user_aiming) {
                print(10, 44, "Aiming source:");
                print(120, 44, user_aim_source.x);
                print(170, 44, user_aim_source.y);


                print(10, 61, "Aiming vector:");
                print(120, 61, compute_applied_force(user_aim_source).x);
                print(170, 61, compute_applied_force(user_aim_source).y);
            }
        }



        // Draw the banderole over everything
        if (bravo_displayed) {
            int bravo_x = 900 - (2901 * bravo_advancement);
            image_draw(bravo, bravo_x, 357);
            bravo_advancement += 0.002;


            // When the bravo animation is finished:
            if (bravo_advancement >= 1) {
                bravo_displayed = false;

                // Switch to the next course
                score += nb_strokes;
                current_course += 1;
                c = get_course(current_course);
                nb_strokes = 0;
            }
        }



        // Print the homepage over everything when its displayed
        if (homepage_y < DIM) {
            image_draw(homepage, 0, homepage_y);
        }






        // Manage user input. Depends on whether we're on the homepage or not:
        // - On the homepage, we only wait for a mouse press to deactivate the homepage
        // - Not on the homepage, we wait for any input used by the game

        if (homepage_lock) {
            if (isMousePressed(SDL_BUTTON_LEFT) || isMousePressed(SDL_BUTTON_RIGHT)) {
                homepage_lock = false;
            }
        }


        else {

            // Remove transparency for the homepage
            if (homepage_y < DIM) {
                homepage_speed += HOMEPAGE_GRAVITY;
                homepage_y += homepage_speed;
            }



            Vec2 push_force = make_vec2(0);
            if (user_aiming) {
                draw_user_line(user_aim_source);

                // If the user released its stroke
                if (!isMousePressed(SDL_BUTTON_LEFT)) {
                    push_force = compute_applied_force(user_aim_source);
                    user_aiming = false;
                    nb_strokes += 1;
                }
            }
            else if (isMousePressed(SDL_BUTTON_LEFT) && user_can_play(c)) {
                user_aiming = true;
                user_aim_source = mouse_pos;
            }


            // The user can grab the ball with the right click. Doing this activate test mode
            if (isMousePressed(SDL_BUTTON_RIGHT)) {

                // Move the ball while it's grabbed
                if (ball_grabbed) {
                    c.ball.p = mouse_pos + grab_delta;
                    c.ball.s = make_vec2(0);
                }

                else {
                    // Check if the mouse is over the ball. If so, grabs it
                    if (norm(mouse_pos - c.ball.p) <= BALL_RADIUS) {
                        test_mode = true;
                        ball_grabbed = true;
                        grab_delta = c.ball.p - mouse_pos;
                    }
                }
            }
            else if (test_mode && ball_grabbed) {
                ball_grabbed = false;
            }



            // The user can reload the course by pressing R
            if (isKeyPressed(SDLK_r)) {
                reset(c);
                nb_strokes = 0;
            }
            
            // The user can enable or disable debug mode by pressing D
            if (isKeyPressed(SDLK_d)) {debug = !debug;}


            // The user can switch levels by using the left and right arrow keys.
            // Doing so will enable test mode, removing the score
            if (isKeyPressed(SDLK_LEFT)) {
                if (current_course == 1) {current_course = COURSE_NUMBER;}
                else {current_course -= 1;}

                c = get_course(current_course);
                test_mode = true;
            }
            if (isKeyPressed(SDLK_RIGHT)) {
                if (current_course == COURSE_NUMBER) {current_course = 1;}
                else {current_course += 1;}

                c = get_course(current_course);
                test_mode = true;
            }



            // If the game is won, go to the next course
            if (c.is_won) {
                // Activate the bravo banderole. It will then switch to the next course
                if (!bravo_displayed) {
                    bravo_advancement = 0;
                    bravo_displayed = true;
                }
            }
            

            // Update game
            update(c, DT, push_force);
        }

        stop = winDisplay();
    }




    winQuit();
    return 0;
}