#include <math.h>
#include "Grapic.h"
using namespace grapic;


const int SPEED_SCALE = 3;


const int MAX_WALLS = 30;
const int MAX_FORCEFIELD = 5;
const int MAX_MAGNET = 5;
const int MAX_MOVING_PART = 11;
const int MAX_PASSAGE_POINTS = 5;

const int BALL_RADIUS = 10;






float deg_to_rad(float deg)
{
    return deg * M_PI / 180.0;
}

float rad_to_deg(float rad)
{
    return rad * 180.0 / M_PI;
}







// VECTOR 2 --------------------------------------------------------------------------------

struct Vec2 {
    float x;
    float y;
};

Vec2 operator+(Vec2 v1, Vec2 v2) {
    Vec2 v;
    v.x = v1.x + v2.x;
    v.y = v1.y + v2.y;

    return v;
}

Vec2 operator-(Vec2 v1, Vec2 v2) {
    Vec2 v;
    v.x = v1.x - v2.x;
    v.y = v1.y - v2.y;

    return v;
}

Vec2 operator*(Vec2 v1, float lbd) {
    Vec2 v;
    v.x = v1.x * lbd;
    v.y = v1.y * lbd;

    return v;
}

Vec2 operator*(float lbd, Vec2 v1) {
    return v1 * lbd;
}

Vec2 operator*(Vec2 v1, Vec2 v2) {
    Vec2 v;
    v.x = (v1.x * v2.x) - (v1.y * v2.y);
    v.y = (v1.x * v2.y) + (v1.y * v2.x);

    return v;
}

Vec2 operator/(Vec2 v1, float lbd) {
    Vec2 v;
    v.x = v1.x / lbd;
    v.y = v1.y / lbd;

    return v;
}

Vec2 operator/(float lbd, Vec2 v1) {
    return v1 / lbd;
}


// Return a Vec2 from the given x and y coordinates
Vec2 make_vec2(float x, float y) {
    Vec2 v;
    v.x = x;
    v.y = y;
    return v;
}

// Return a Vec2 with the same value for both of its coordinates
Vec2 make_vec2(float xy) {
    Vec2 v;
    v.x = xy;
    v.y = xy;
    return v;
}

// Return a Vec2 from the given polar coordinates (in degrees)
Vec2 make_polar_vec2(float mod, float theta) {
    float rad = deg_to_rad(theta);
    Vec2 c;
    c.x = mod * cos(rad);
    c.y = mod * sin(rad);

    return c;
}


// Return the norm of the vector
float norm(Vec2 v) {
    return sqrt((v.x * v.x) + (v.y * v.y));
}

// Return a normalized version of the vector
Vec2 normalized(Vec2 v) {

    if (v.x == 0 && v.y == 0) {return v;}

    Vec2 nv;
    nv.x = v.x / norm(v);
    nv.y = v.y / norm(v);

    return nv;
}


// Return dot product of the two vectors
float dot(Vec2 v1, Vec2 v2) {
    return v1.x * v2.x + v1.y * v2.y;
}



// Rotate the point p around the point center of the given angle
Vec2 rotate(Vec2 p, float angle, Vec2 center) {
    Vec2 rotation_vector = make_polar_vec2(1, -angle);

    p = p - center;
    p = p * rotation_vector;
    p = p + center;

    return p;
}






// Return the normal of the wall. Its direction depends on p1 and p2
Vec2 normal(Vec2 p1, Vec2 p2) {
    Vec2 perp = make_vec2(p2.y - p1.y, p1.x - p2.x);
    return normalized(perp);
}




// Used for comparison of walls when checking for collisions
bool operator==(Vec2 v1, Vec2 v2) {return v1.x == v2.x && v1.y == v2.y;}
bool operator!=(Vec2 v1, Vec2 v2) {return !(v1 == v2);}








// Wall (part 1) ---------------------------------------------------------------------------

// Predeclaring the Wall struct for it to be used in the Ball struct
struct Wall {
    Vec2 p1;
    Vec2 p2;

    Vec2 speed;

    float length;
    int width;

    unsigned char color[3];   // display color (default red)
};






// BALL     --------------------------------------------------------------------------------





struct Ball {
    Vec2 p;                   // Position
    Vec2 s;                   // Speed
    Vec2 f;                   // Forces
    float radius;             // radius of the particle, in pixels

    float bounce;             // 1: will bounce without loosing energy, 0: will not bounce
    float grip;               // 0: will never loose energy while rolling, 1: won't roll

    unsigned char color[3];   // display color (default white)

    bool is_colliding;        // whether the ball is colliding with a wall on this frame or not
    Wall last_hit;            // Wall that was hit last. Prevent the ball to be stuck in a fixed wall
};




// Create a new Ball at the given coordinates 
void init(Ball &b, float x, float y, float radius) {
    b.radius = radius;

    b.p.x = x;
    b.p.y = y;

    b.s = make_vec2(0, 0);
    b.f = make_vec2(0.0);

    // Default bounce and grip
    b.bounce = 0.9;
    b.grip = 0.0025;

    // Default color (white like a golfball)
    b.color[0] = 255;
    b.color[1] = 255;
    b.color[2] = 255;
}








// Draw the ball on screen
void draw(Ball b, bool debug) {

    color(b.color[0], b.color[1], b.color[2]);
    if (norm(b.s) != 0 || norm(b.f) != 0) {
        color(b.color[0] - 60, b.color[1] - 60, b.color[2] - 60);
    }

    circleFill(b.p.x, b.p.y, b.radius);

    // Draw speed vector if specified
    if (debug) {
        color(0, 0, 255);
        Vec2 p;
        p = b.p + (b.s * SPEED_SCALE);
        line(b.p.x, b.p.y, p.x, p.y);
    }
}





// update the attributes of the particle
// push is the force applied by the player to the ball
// grip is the computed grip of the ball depending on its environment
void update(Ball &b, float DT, float grip) {
   
    b.s = (b.s + b.f) * (1 - (grip));            // Update speed
    b.p = b.p + (b.s * DT);                           // Update position

    // Force the ball to stop once it's almost finished moving.
    // Doesn't do that if the ball is undergo forces
    if (norm(b.s) * DT <= 3*DT && norm(b.f) == 0) {b.s = make_vec2(0);}
}

















// WALL (part two)    ------------------------------------------------------------------------





// Initialize a new wall
void init(Wall &w, Vec2 p1, Vec2 p2, int width) {
    w.p1 = p1;
    w.p2 = p2;

    w.speed = make_vec2(0);

    w.width = width;

    w.color[0] = 191;
    w.color[1] = 54;
    w.color[2] = 65;

    w.length = norm(w.p2 - w.p1);
}


// Used for comparison of walls when checking for collisions
bool operator==(Wall w1, Wall w2) {return w1.p1 == w2.p1 && w1.p2 == w2.p2 && w1.width == w2.width;}
bool operator!=(Wall w1, Wall w2) {return !(w1 == w2);}


// Return the middle of the wall
Vec2 middle(Wall w) {
    return w.p1 + (w.p2 - w.p1) / 2;
}


// Return the normal of the wall. Its direction depends on p1 and p2
Vec2 normal(Wall w) {
    return normal(w.p1, w.p2);
}



// Draw a fat line using circleFill
void fat_line(int x1, int y1, int x2, int y2, int width) {
    int dist = sqrt((y2 - y1)*(y2 - y1) + (x2 - x1)*(x2 - x1));

    int i;
    for (i=0; i<dist; i+=width) {
        int x = x1 + i * (x2-x1) / dist;
        int y = y1 + i * (y2-y1) / dist;

        circleFill(x, y, width);
    }
}





// Return the nearest point of the line to a specific point
Vec2 nearest_point(Wall w, Vec2 p) {
    // if the perpendicular projection of p on w is between the 2 ends of w
    if (dot(w.p1 - p, w.p1 - w.p2) > 0 && dot(w.p2 - p, w.p2 - w.p1) > 0) {
        Vec2 v = normalized(w.p2 - w.p1);
        float d = dot((p - w.p1), v);
        return w.p1 + v * d;
    }

    // if the projection is outside, take the nearest of the 2 ends
    else if (norm(w.p2 - p) < norm(w.p1 - p)) {return w.p2;}
    else {return w.p1;}
}




// Return the minimum distance between the ball center point the wall
float minimum_distance(Ball b, Wall w) {
    Vec2 nearest = nearest_point(w, b.p);
    float min_dist = norm(nearest - b.p);

    return min_dist;
}



// Return if the ball intersect with the wall
bool intersect(Ball b, Wall w) {
    float min_dist = minimum_distance(b, w);
    float max_dist = std::max(norm(w.p2 - b.p), norm(w.p1 - b.p));

    return abs(min_dist) <= b.radius + w.width && abs(max_dist) >= b.radius + w.width;
}



// Collision of a solid projectile with a given speed with the wall
Vec2 collision_impulse(Wall w, Vec2 speed) {
    Vec2 normal_vec = normal(w);

    Vec2 normal_vec_perp = make_vec2(normal_vec.y, -normal_vec.x);

    Vec2 speed_perp = dot(speed, normal_vec_perp) * normal_vec_perp;
    Vec2 speed_par = dot(speed, normal_vec) * normal_vec;

    Vec2 speed_par_after_collision = speed_par - 2 * dot(speed_par, normal_vec) * normal_vec;

    Vec2 speed_after_collision = speed_perp + speed_par_after_collision;

    return speed_after_collision;
}




// Compute the speed vector that the ball b will have after colliding with the wall w
Vec2 collision_with_fixed_wall(Ball b, Wall w) {
    return collision_impulse(w, b.s);
}






// Draw the wall on the screen. Used for debugging, as normally the walls
// are displayed in the background image.
void draw(Wall w) {
    color(0, 0, 255);
    line(w.p1.x, w.p1.y, w.p2.x, w.p2.y);

    // draw the normal
    color(0, 0, 255);
    Vec2 mid = middle(w);
    Vec2 other1 = mid + normal(w) * w.width;
    Vec2 other2 = mid - normal(w) * w.width;

    line(other1.x, other1.y, other2.x, other2.y);

    // Draw the wall speed vector if it's =/= than 0
    if (norm(w.speed) > 0) {
        color(255, 255, 255);
        fontSize(10);
        Vec2 center = (w.p2 + w.p1) / 2;
        print(center.x - 45, center.y + 20, w.speed.x);
        print(center.x - 10, center.y + 20, w.speed.y);
    }
    
}






// FORCEFIELD   ----------------------------------------------------------------------------



// Represent a zone where the ball is subject to a given force, like an incline or a speed boost, for example.
// the zone can only be a quadrilateral
struct Forcefield {
    Vec2 p1;
    Vec2 p2;
    Vec2 p3;
    Vec2 p4;

    Vec2 force_vector;
};


void init(Forcefield &ff, Vec2 p1, Vec2 p2, Vec2 p3, Vec2 p4, Vec2 force) {
    ff.p1 = p1;
    ff.p2 = p2;
    ff.p3 = p3;
    ff.p4 = p4;

    ff.force_vector = force;
}



// Check if the ball is in the forcefield
bool is_ball_affected(Forcefield ff, Ball b) {
    // Divide the quadrilateral in 2 triangle and use grapic function to check if the ball
    // is in one of the triangles;
    bool t1 = isInTriangle(b.p.x, b.p.y, ff.p1.x, ff.p1.y, ff.p2.x, ff.p2.y, ff.p3.x, ff.p3.y);
    bool t2 = isInTriangle(b.p.x, b.p.y, ff.p1.x, ff.p1.y, ff.p3.x, ff.p3.y, ff.p4.x, ff.p4.y);

    return t1 || t2;
}





// Draw the forcefield on the screen. Used for debugging
void draw(Forcefield ff) {
    color(255, 255, 0);
    line(ff.p1.x, ff.p1.y, ff.p2.x, ff.p2.y);
    line(ff.p2.x, ff.p2.y, ff.p3.x, ff.p3.y);
    line(ff.p3.x, ff.p3.y, ff.p4.x, ff.p4.y);
    line(ff.p4.x, ff.p4.y, ff.p1.x, ff.p1.y);

    line(ff.p1.x, ff.p1.y, ff.p3.x, ff.p3.y);

    int center_x = (ff.p1.x + ff.p2.x + ff.p3.x + ff.p4.x) / 4;
    int center_y = (ff.p1.y + ff.p2.y + ff.p3.y + ff.p4.y) / 4;

    line(center_x, center_y, center_x + ff.force_vector.x * SPEED_SCALE, center_y + ff.force_vector.y * SPEED_SCALE);
}













// MOVING_PART  ----------------------------------------------------------------------------



// a part that can move along a path and rotate at a constant speed
struct MovingPart {
    Wall wall[MAX_WALLS];
    int nb_walls;

    float speed;                   // speed of the moving part along the path (pixels per seconds)
    float advancement;             // Where the part is in its path, in pixels

    Vec2 speed_vector;             // Vec2 that represents the speed of the part

    Vec2 path[MAX_PASSAGE_POINTS];
    float passage_point_weight[MAX_PASSAGE_POINTS]; // The %age the path between point (n) and point (n-1) compared to the total length.
    int nb_passage_points;

    bool loop;                     // Whether the part should go to the first point when reaching the end, or go the other direction
    int direction;                 // 1: goes from p(n) to p(n+1);   -1: goes from p(n) to p(n-1)

    float angle;                   // in degrees
    float rotation_speed;          // in degrees per second

    Vec2 delta_move;               // The difference of position between 2 frames. Updated at each frame

    Image img;
    int dimx;
    int dimy;
};




// Compute the relative length of each path segment compared to the total length.
// Return the total length of the path
float compute_path_length(MovingPart &m) {
    int i;
    float total_length = 0;

    m.passage_point_weight[0] = 0;
    for (i=1; i<m.nb_passage_points; i++) {
        float length = norm(m.path[i] - m.path[i-1]);
        total_length += length;
        m.passage_point_weight[i] = length;
    }

    // Prevent division by 0
    if (total_length != 0) {
        for (i=1; i<m.nb_passage_points; i++) {
            m.passage_point_weight[i] /= total_length;
        }
    }

    return total_length;
}



void init(MovingPart &m, Image img, int dimx, int dimy, Vec2 starting_point, bool loop, float move_speed, float rotation_speed) {
    m.nb_walls = 0;
    m.nb_passage_points = 1;

    m.path[0] = starting_point;

    m.advancement = 0;
    m.direction = 1;

    m.speed = move_speed;
    m.delta_move = make_vec2(0);

    m.loop = loop;
    m.img = img;

    m.dimx = dimx;
    m.dimy = dimy;

    m.angle = 0;
    m.rotation_speed = rotation_speed;
}





// Add a passage point to the moving part
void add_point(MovingPart &m, Vec2 point) {
    if (m.nb_passage_points < MAX_PASSAGE_POINTS) {
        m.path[m.nb_passage_points] = point;
        m.nb_passage_points += 1;

        compute_path_length(m);
    }
}


// Add a wall to the moving part. The coordinates of the walls are the ones for m.percent = 0.
void add_wall(MovingPart &m, Wall w) {
    if (m.nb_walls < MAX_WALLS) {
        m.wall[m.nb_walls] = w;
        m.nb_walls += 1;
    }
}



// Return the coordinates of the moving part
Vec2 get_pos(MovingPart m) {
    if (m.nb_passage_points == 1) {return m.path[0];}

    float percent = m.advancement * 1.0 / compute_path_length(m);
    float current_percent = 0;
    int path_segment = 0;

    while (current_percent <= percent) {
        path_segment += 1;
        current_percent += m.passage_point_weight[path_segment];
    }
    current_percent -= m.passage_point_weight[path_segment];

    // We know the requested position is between m.path[path_segment - 1] and m.path[path_segment]
    // the distance between those 2 points represents m.passage_point_weigth[path_segment] %age of the total distance

    float interpolation_step = (percent - current_percent) / m.passage_point_weight[path_segment];

    return m.path[path_segment - 1] + (m.path[path_segment] - m.path[path_segment - 1]) * interpolation_step;
}



// Move every walls of the moving part by the delta vector
void move_walls(MovingPart &m, Vec2 delta, float angle, Vec2 center) {
    int i;
    for (i=0; i<m.nb_walls; i++) {

        // Move the walls
        m.wall[i].p1 = m.wall[i].p1 + delta;
        m.wall[i].p2 = m.wall[i].p2 + delta;

        // Rotate the walls
        m.wall[i].p1 = rotate(m.wall[i].p1, angle, center);
        m.wall[i].p2 = rotate(m.wall[i].p2, angle, center);

        m.wall[i].speed = normalized(delta) * m.speed;
    }
}



// update the position and angle of the moving part
void update(MovingPart &m, float DT) {
    float total_length = compute_path_length(m);
    Vec2 last_pos = get_pos(m);

    m.angle += m.rotation_speed * DT;
    

    m.advancement += m.direction * m.speed * DT;

    if (m.advancement > total_length) {
        if (m.loop) {m.advancement = m.advancement - total_length;}
        else {
            m.advancement = (2 * total_length)- m.advancement;
            m.direction *= -1;
        }
    }

    else if (m.advancement < 0) {
        if (m.loop) {m.advancement += total_length;}
        else {
            m.advancement *= -1;
            m.direction *= -1;    
        }
    }

    Vec2 new_pos = get_pos(m);

    m.delta_move = (new_pos - last_pos) / DT;

    move_walls(m, new_pos - last_pos, m.rotation_speed * DT, get_pos(m));
}




// Compute the speed vector of the given point, considering it's part of
// the moving part
Vec2 get_speed_of_point(MovingPart mv, Vec2 point) {
    // Compute the speed of the collision point on the moving part
    Vec2 point_speed = mv.delta_move;

    // Add the linear speed of the point
    float angular_speed = deg_to_rad(mv.rotation_speed);
    float radius = norm(point - get_pos(mv));       // Distance between the ball and the center of rotation
    point_speed = point_speed + normal(get_pos(mv), point) * angular_speed * radius;
    

    return point_speed;
}





Vec2 collision_with_moving_wall(Ball &b, MovingPart mv, Wall w) {
    Vec2 collision_point = nearest_point(w, b.p);

    // Compute the collision, like if the wall was fixed. For that, we add the opposite of the collision point speed to the ball speed
    Vec2 collision_speed = b.s - get_speed_of_point(mv, collision_point);

    return collision_impulse(w, collision_speed) * 1.01;
}




void draw(MovingPart m, bool debug) {
    int i;
    Vec2 pos = get_pos(m);

    if (debug) {        
        color(255, 255, 255);
        for (i=1; i<m.nb_passage_points; i++) {
            line(m.path[i-1].x, m.path[i-1].y, m.path[i].x, m.path[i].y);
        }
    }

    // Draw the image centered
    image_draw(m.img, pos.x - (m.dimx/2), pos.y - (m.dimy/2), -1, -1, m.angle);

    // Draw the walls if debug is enabled
    if (debug) {
        for (i=0; i<m.nb_walls; i++) {
            draw(m.wall[i]);
        }
    }
}


















// COURSE   --------------------------------------------------------------------------------

struct Course {
    Ball ball;

    Wall walls[MAX_WALLS];
    int nb_walls;

    Forcefield ff[MAX_FORCEFIELD];
    int nb_ff;

    MovingPart mov_part[MAX_MOVING_PART];
    int nb_moving_part;

    Vec2 ball_pos;
    Vec2 last_safe_pos;

    Vec2 hole_pos;

    Image background;

    bool is_won;

    int course_nb;
};


// Init a new course without any walls
void init(Course &c, Vec2 ball_pos, Vec2 hole_pos, Image bg, int course_nb) {
    init(c.ball, ball_pos.x, ball_pos.y, BALL_RADIUS);
    c.nb_walls = 0;
    c.nb_ff = 0;
    c.nb_moving_part = 0;

    c.ball_pos = ball_pos;
    c.last_safe_pos = ball_pos;

    c.hole_pos = hole_pos;

    c.is_won = false;

    c.background = bg;
    
    c.course_nb = course_nb;
}



// Add a new wall to the course
void add_wall(Course &c, Wall w) {
    if (c.nb_walls < MAX_WALLS) {
        c.walls[c.nb_walls] = w;
        c.nb_walls += 1;
    }
}


// Add a new forcefield to the course
void add_forcefield(Course &c, Forcefield ff) {
    if (c.nb_ff < MAX_FORCEFIELD) {
        c.ff[c.nb_ff] = ff;
        c.nb_ff += 1;
    }
}



// Add a new moving part to the course
void add_moving_part(Course &c, MovingPart m) {
    if (c.nb_moving_part < MAX_MOVING_PART) {
        c.mov_part[c.nb_moving_part] = m;
        c.nb_moving_part += 1;
    }
}



// Reset the course
void reset(Course &c) {
    c.ball.p = c.ball_pos;
    c.ball.s = make_vec2(0);
    c.ball.f = make_vec2(0);
    c.is_won = false;
}


// Put in "color" the color of the pixel "undeer" the ball, from the course image
void get_land_color(Course &c, int color[3]) {
    for (int i = 0; i<3; i++) {
        color[i] = image_get(c.background, c.ball.p.x, c.ball.p.y, i);
    }
}



// Return the grip of the ball, taking into account the environment (color of the pixel in the center of the ball)
float get_grip(Course &c) {
    int color[3];
    get_land_color(c, color);


    // Check for specific colors, representing specific surfaces with grip modifiers
    // 213 191 122: Sand, grip modifier: 3.5
    if (color[0] == 213 && color[1] == 191 && color[2] == 122) {
        return c.ball.grip * 3.5;
    }


    // If no modifier was applied, return default grip
    return c.ball.grip;
}


float get_bounce(Course &c) {
    int color[3];
    get_land_color(c, color);


    // 60 77 98: Slowing zone (in space levels), bounce: 0.9
    if (color[0] == 60 && color[1] == 77 && color[2] == 98) {
        return 0.01;
    }


    // If no modifier was applied, return default grip
    return c.ball.bounce; 
}




// Print text used for debug
void print_debug_text(Course c) {
    color(255, 255, 255);
    fontSize(15);

    print(10, 10, "Ball position: ");
    print(108, 10, round(c.ball.p.x));
    print(160, 10, round(c.ball.p.y));

    print(10, 27, "Force on ball: ");
    print(108, 27, round(c.ball.f.x));
    print(160, 27, round(c.ball.f.y));
}




// Draw the course
void draw(Course c, bool debug) {
    int i;

    // Draw the bg
    image_draw(c.background, 0, 0, 900, 900);

    // Draw the moving parts
    for (i=0; i<c.nb_moving_part; i++) {
        draw(c.mov_part[i], debug);

        if (debug) {
            int j;
            for (j=0; j<c.mov_part[i].nb_walls; j++) {
                // Draw the nearest point from the ball of each wall
                Vec2 nearest = nearest_point(c.mov_part[i].wall[j], c.ball.p);
                color(255, 255, 0);
                circleFill(nearest.x, nearest.y, 3);

                // Draw the speed vector of the nearest point to the ball
                Vec2 nearest_speed = get_speed_of_point(c.mov_part[i], nearest);

                line(nearest.x, nearest.y, nearest.x + nearest_speed.x * SPEED_SCALE, nearest.y + nearest_speed.y * SPEED_SCALE);
            }
        }
    }


    // Draw the ball
    if (!c.is_won) {
        draw(c.ball, debug);
    }


    if (debug) {
        // Draw the walls
        for (i=0; i<c.nb_walls; i++) {
            draw(c.walls[i]);

            // Draw the nearest point from the ball of each wall
            Vec2 nearest = nearest_point(c.walls[i], c.ball.p);
            color(255, 255, 0);
            circleFill(nearest.x, nearest.y, 3);
        }

        // Draw the forcefields
        for (i=0; i<c.nb_ff; i++) {
            draw(c.ff[i]);
        }

        color(0, 0, 255, 125);
        circleFill(c.hole_pos.x, c.hole_pos.y, BALL_RADIUS);

        print_debug_text(c);
    }
}



void update(Course &c, float DT, Vec2 push_force) {
    if (!c.is_won) {
        int i;

        c.ball.f = push_force;

        // Apply the effect of forcefields to the ball
        for (i=0; i<c.nb_ff; i++) {
            if (is_ball_affected(c.ff[i], c.ball)) {
                c.ball.f = c.ball.f + (c.ff[i].force_vector * DT);

                Wall w; init(w, make_vec2(0), make_vec2(0), 1); // not a real wall, only used to reset the last_hit variable
                c.ball.last_hit = w;
            }
        }

        /*
        // Apply the effect of magnets to the ball
        for (i=0; i<c.nb_magnet; i++) {
            if (norm(c.ball.p - c.magnet[i].p) <= c.magnet[i].radius) {
                c.ball.is_in_ff = true; // A magnet acts like a forcefield

                float force = c.magnet[i].power / pow(norm(c.ball.p - c.magnet[i].p), 1.2);

                force_on_ball = force_on_ball + normalized(c.magnet[i].p - c.ball.p) * force;
            }
        }
        */


        // Update the ball
        update(c.ball, DT, get_grip(c));



        // Update the moving parts
        for (i=0; i<c.nb_moving_part; i++) {
            update(c.mov_part[i], DT);
        }



        // Check if the ball reached a forbidden zone, like water
        int terrain_color[3];
        get_land_color(c, terrain_color);

        // water
        if (terrain_color[0] == 0 && terrain_color[1] == 128 && terrain_color[2] == 183) {
            reset(c);
            c.ball.p = c.last_safe_pos;
        }
    




        // Check if the ball is stopped. If so, set the last safe pos to its pos
        // and reset the last_hit wall
        if (norm(c.ball.s) == 0) {
            c.last_safe_pos = c.ball.p;

            Wall w; init(w, make_vec2(0), make_vec2(0), 1); // not a real wall, only used to reset the last_hit variable
            c.ball.last_hit = w;
        }



        // Check if the ball collides with a wall
        c.ball.is_colliding = false;

        Wall colliding_with;

        bool colliding_moving_part = false;
        int wall_owner_id;
        
        for (i=0; i<c.nb_walls; i++) {                              // Walls of the course

            bool ball_intersect = intersect(c.ball, c.walls[i]);

            if (ball_intersect) {
                c.ball.is_colliding = true;
                colliding_with = c.walls[i];
            }
        }


        for (i=0; i<c.nb_moving_part; i++) {                        // Walls of the moving parts
            int j;
            for (j=0; j<c.mov_part[i].nb_walls; j++) {

                bool ball_intersect = intersect(c.ball, c.mov_part[i].wall[j]);

                if (ball_intersect) {
                    c.ball.is_colliding = true;
                    colliding_moving_part = true;

                    colliding_with = c.mov_part[i].wall[j];
                    wall_owner_id = i;
                }

            }
        }


        // Check if the ball already hit that wall on the last frame. If so, ignore that
        // as it can cause a stuck ball.
        if (c.ball.is_colliding && c.ball.last_hit != colliding_with) {

            // first, move the ball out of the wall.
            Vec2 collision_point = nearest_point(colliding_with, c.ball.p);
            float dist = norm(c.ball.p - collision_point);
            c.ball.p = c.ball.p + normalized(c.ball.p - collision_point) * ((colliding_with.width + BALL_RADIUS)- dist);

                        
            if (colliding_moving_part) {
                c.ball.s = collision_with_moving_wall(c.ball, c.mov_part[wall_owner_id], colliding_with); // Not bouncing with moving walls to prevent some bugs
            }
            else {
                c.ball.s = collision_with_fixed_wall(c.ball, colliding_with) * get_bounce(c);
            }

            c.ball.last_hit = colliding_with;
        }


        // Check if the ball went into the hole
        float hole_dist = norm(c.ball.p - c.hole_pos);
        if (hole_dist <= BALL_RADIUS && norm(c.ball.s) < 60) {
            c.is_won = true;
        }
    }
}