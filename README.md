# Minigolf - Projet LIFAMI
JOSSERAND Léo


## Comment jouer

Le but du minigolf est de mettre la balle dans le trou en un minimum de coups possibles.
Pour lancer la balle, il faut cliquer n'importe où sur l'écran et bouger sa souris dans la direction inverse à où l'on veut faire partir la balle. Plus on chargera le lancé, plus le trait représentant le tir sera de couleur vive et plus le coup sera puissant.

### Raccourcis disponibles:

| Touche | Description |
| ------ | ----------- |
| R | Réinitialise le niveau (remet la balle au départ et le nombre de coups à 0) |
| D | Active ou désactive le mode debug |
| Flèche gauche/droite | Change de niveau. Active le mode test |


### Mode debug

Le mode debug permet de visualiser certaines forces et les objets invisibles, comme les champs de force, les murs, etc. Cela permet notamment de vérifier que les objets sont bien placés par rapport à l'image du niveau.

Le mode debug affiche:
- Le vecteur vitesse de la balle
- La position des murs et du trou (pour vérifier qu'ils coïncident bien avec l'image de fond)
- Le vecteur normal des murs qui correspondent également à la largeur des murs (pour les mêmes besoin de vérification de cohérence)
- Le trajet des objets mouvants
- Le point le plus proche de la balle pour chaque mur
- Les champs de forces et leur vecteur
- Les aimants
- Les coordonnées du vecteur vitesse de chaque mur
- Le vecteur vitesse du point le plus proche de la balle pour chaque mur d'un objet mouvant
- Ainsi que divers valeurs numériques


### Mode tranquille

Le mode tranquille permet de visiter les différents niveaux sans avoir à les réussir. Il s'active automatiquement en appuyant sur les touches flèche gauche ou flèche droite. Cela désactive le comptage du nombre de coups et de score.

De plus, le mode tranquille permet également de déplacer la balle à sa guise. Il suffit de l'attraper en faisant clic droit dessus, et de la déplacer toute en restant appuyé sur le clic droit.


## Explication du code

### Composition d'un terrain

Un terrain, représenté par la structure `Course`, contient :
- Des murs, structure `Wall`
- une balle, structure `Ball`
- des champs de forces, structure `Forcefield`
- des aimants, structure `Magnet`
- des objets mouvants, structure `MovingPart`

La structure `Course` gère la mise à jour et l'interaction entre chacun de ces éléments et la balle.



### Affichage

Chaque trou est représenté par une image qui contient déjà tous les éléments immobiles (les murs, le trou, etc.). Les différents objets sont créés de manière à ce qu'ils soit exactement au même endroit que l'image, ce qui donne l'illusion que la balle rebondit sur le mur.


## Document utilisés

- Point le plus proche sur une ligne: https://forum.unity.com/threads/how-do-i-find-the-closest-point-on-a-line.340058/


## Evolution

### Les murs

Au départ, les murs était affichés de la même manière que le "trait de puissance" (affiché lorsque le joueur tire), c'est-à-dire en dessinant des disques le long d'une ligne. Cependant, il s'avère que cette technique est très lente et rendait le jeu injouable dès qu'il y avait plus de 3 murs. Le fait d'afficher une seule image pour tout le niveau est bien moins demandeur en ressource, et permet d'avoir un jeu plus joli car on peut afficher plus de détails.


### Calcul des collisions, du point d'attaque

Au départ, le calcul du point d'impact entre la balle et un mur était calculé grâce à l'aire du triangle formé par les extrémités du mur et la balle. Cela ne calculait pas à proprement parler le point d'impact mais la distance à ce point d'impact. Cette méthode fonctionnait, mais durant l'implémentation des obstacles mouvants et tournants, j'ai eu besoin de calculer le point d'impact (sous forme de `Vec2`). En implémentant cela, l'algorithme pour trouver la distance au point le plus proche n'avait plus besoin de l'aire d'un triangle: il suffit de calculer le point le plus proche, puis la distance à ce point.




## Historique

### 29/04
- Ajout des niveaux 7 et 8.
- Amélioration de la collision de la balle avec les obstacles mouvants.
- Changement du système anti-blocage de la balle dans un mur. Le système est maintenant par balle et non plus global à la structure `Course` (pour potentiellement supporter plusieurs balles dans le futur).
- Le mode debug n'affiche plus que la vitesse des murs dont la vitesse est non nulle.



### 06/05
- Suppression des Aimants et de l'ancien trou 5: pas amusant et peu utilisés.
- Ajout du niveau 8.
- Au lieu d'utiliser un booléen `b.is_in_ff` pour tester que la balle n'est pas dans un champs de force (car on ne peut pas jouer une balle qui subit une force), j'utilise désormais `b.f`. Cela permet de considérer que la balle ne subit pas de force si elle se trouve dans 2 champs de forces s'annulant (utilisé pour le niveau 8).
- Début du dernier trou (9).



### 13/05
- Ajout d'une page d'accueil détaillant les contrôles et d'une banderole "BRAVO" lorsqu'un niveau est réussi.